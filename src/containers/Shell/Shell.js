import React, { Component } from 'react';
import classes from './Shell.module.css';
import { Route } from 'react-router-dom';
import DCLogin from '../DCLogin/DCLogin';
import DCChat from '../DCChat/DCChat';

export default class Shell extends Component {
  render () {
    return (
      <main className={classes.main_content}>
        <Route path="/chat" component={DCChat}></Route>
        <Route path="/" exact component={DCLogin}></Route>
      </main>
    );
  };
}
