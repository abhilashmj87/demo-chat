import React, { Component } from 'react';
import classes from './DCLogin.module.css';
import DCButton from '../../components/DCButton/DCButton';
import DCInput from '../../components/DCInput/DCInput'
import DCDrawer from '../../components/DCDrawer/DCDrawer';
import { withRouter } from 'react-router';

class DCLogin extends Component {
  state = {
    user: '',
  };

  navigateToChannels = () => {
    this.props.history.push({
      pathname: '/chat',
    },
    {
      username: this.state.user,
    });
  };

  updateName = (ev) => {
    this.setState({
      user: ev.target.value
    });
  };

  render() {
    return (
      <div className={'dc-core-flex ' + classes.loginContainer}>
        <DCDrawer theme="primary" className="dc-core-flex-item dc-core-flex-3 dc-core-md-hidden dc-core-padding-sm">
          <h1 className="dc-core-align-center">Demo Chat</h1>
          <h4 className="dc-core-align-center">The chat app !</h4>
          <img alt="collaboration" className={classes.loginIntroImg} src="https://image.flaticon.com/icons/png/512/3097/3097792.png" />
          <div className="dc-core-margin-lg dc-core-padding-lg">Making it insanely easy to talk and hang out together !</div>
        </DCDrawer>
        <div className="dc-core-flex-item dc-core-padding-sm dc-core-hide-overflow">
          <h1 className="dc-core-align-center dc-core-lg-hidden">Demo Chat</h1>
          <h4 className="dc-core-align-center dc-core-lg-hidden">The chat app !</h4>
          <div className={'dc-core-flex dc-core-flex-align-items-center dc-core-flex-content-center dc-core-flex-cloumn-direction ' + classes.loginContainer}>
            <div className="dc-core-flex-item dc-core-flex-column-1">
              <form>
                <DCInput placeholder="Enter your name" onchange={this.updateName}></DCInput>
              </form>
            </div>
            <div className="dc-core-flex-item dc-core-flex-column-1">
              <DCButton size="md" disable={!this.state.user} onclick={this.navigateToChannels}>Join the Demo Chat!</DCButton>
            </div>
          </div>
        </div>
      </div>
    );
  };
}

export default withRouter(DCLogin)
