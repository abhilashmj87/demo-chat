import React, { Component } from 'react';
import classes from './DCRoom.module.css';
import DCHttp from '../../components/DCHttp/DCHttp';
import DCPageHeader from '../../components/DCPageHeader/DCPageHeader';
import DCInput from '../../components/DCInput/DCInput';
import DCButton from '../../components/DCButton/DCButton';
import DCMessage from '../../components/DCMessage/DCMessage';

export default class DCRoom extends Component {
  constructor(props) {
    super(props);
    this.state = {
    roomDetails: {},
    messages: [],
    currentActiveRoom: '',
    }
    this.messagesComponent = [];
    this.userMsg = "";
  }

  fetchMessagesFromRoom = (room, roomDetails) => {
    DCHttp({
      type: 'get',
      url: 'http://localhost:8080/api/rooms/' + room + '/messages',
    }).then ( resolve => {
      this.messagesComponent = [];
      resolve.forEach(msg => {
        this.messagesComponent.push(<DCMessage message={msg} key={msg.id} currentUser={this.props.username}></DCMessage>)
      });
      this.setState({roomDetails: {...roomDetails}, messages: [...resolve], currentActiveRoom: room});
    }).catch ( reject => {
      console.error(reject);
    });
  };

  fetchRoomDetails = (room) => {
    DCHttp({
      type: 'get',
      url: 'http://localhost:8080/api/rooms/' + room,
    }).then ( resolve => {
        this.fetchMessagesFromRoom(room, resolve);
    }).catch ( reject => {
      console.error(reject);
    });
  };

  componentDidMount() {
    // TODO: Add better error handling to check if activeRoom is invalid or not.
    this.fetchRoomDetails(this.props.activeRoom || 0);
  };

  componentDidUpdate() {
    if (this.state.currentActiveRoom !== this.props.activeRoom) {
      this.fetchRoomDetails(this.props.activeRoom);
      this.fetchMessagesFromRoom(this.props.activeRoom);
    }
  };
  
  handleInputChange = (ev) => {
    this.userMsg = ev.target.value;
  }

  sendMessage = () => {
    const userMsgId = Date.now();
    let newMsg = {
      id: userMsgId,
      name: this.props.username,
      message: this.userMsg,
      reaction: null,
    };
    const newSetOfMessages = this.state.messages;
    newSetOfMessages.push(newMsg);
    DCHttp({
      type: 'post',
      url: 'http://localhost:8080/api/rooms/' + (this.props.activeRoom || 0) + '/messages',
      data: newMsg,
    }).then ( resolve => {
      this.messagesComponent.push(<DCMessage message={newMsg} key={newMsg.id} currentUser={this.props.username}></DCMessage>);
      this.setState({
        messages: newSetOfMessages,
      })
    }).catch ( reject => {
      console.error(reject);
    });

  }

  render() {
    return (
      <div className={classes.activeRoom}>
        <DCPageHeader className={classes.stickyHeader} headerTitle={this.state.roomDetails.name} headerSubtitle={this.state.roomDetails.users}></DCPageHeader>
        <div className={classes.mainContent}>
          <div>
            {this.messagesComponent}
          </div>
        </div>
        <div className={classes.stickyFooter}>
          <div className="dc-core-flex dc-core-flex-align-items-center">
            <div className="dc-core-flex-item dc-core-flex-10">
              <DCInput className={classes.fullWidthInput} placeholder={'Message ' + this.state.roomDetails.name} onchange={this.handleInputChange} template="textarea">
              </DCInput>
            </div>
            <div className="dc-core-padding-sm"></div>
              <DCButton size="sm" onclick={this.sendMessage}>Send</DCButton>
          </div>
        </div>
      </div>
    );
  }
}
