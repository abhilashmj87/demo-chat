import React, { Component } from 'react';
import classes from './DCChat.module.css';
import DCDrawer from '../../components/DCDrawer/DCDrawer';
import DCHttp from '../../components/DCHttp/DCHttp';
import DCList from '../../components/DCList/DCList';
import DCRoom from '../DCRoom/DCRoom';
import DCTimeRenderer from '../../components/DCTimeRenderer/DCTimeRenderer'

export default class DCChat extends Component {
  constructor(props) {
    super(props);
    this.keyid = 0;
    this.state = {
      user: '',
      activeRoom: '',
      rooms: [],
    };
  };


  componentDidMount() {
    DCHttp({
      type: 'get',
      url: 'http://localhost:8080/api/rooms'
    }).then ( resolve => {
      this.setState({
        activeRoom: resolve[0].id,
        user: this.props.history.location.state.username,
        rooms: resolve,
      });
    });
  };

  toggleRoomsDrawer = () => {
    document.querySelector(`.${classes.toggleHelper}`).classList.toggle('dc-core-md-hidden');
    document.querySelector(`.${classes.toggleDrawerOnMobile}`).classList.toggle(classes.toggleHelperDrawerOpen);
  }

  changeActiveRoom = (ev) => {
    this.setState({activeRoom: ev.target.getAttribute('dataKey')});
    this.toggleRoomsDrawer();
  }

  render() {
    return (
      <div className={'dc-core-flex ' + classes.chatContainer}>
        <DCDrawer theme="primary" initialAnimation="animate" className={"dc-core-flex-item dc-core-flex-3 dc-core-flex-md-9 dc-core-md-hidden " + classes.toggleHelper}>
          <div title={this.state.user} className={classes.userNameDiv}>{this.state.user}</div>
          <DCTimeRenderer textBeforeTime="Online from "></DCTimeRenderer>
          <div  className={classes.roomList}>
            <DCList data={this.state.rooms} activeItem={this.state.activeRoom} action={this.changeActiveRoom}></DCList>
          </div>
        </DCDrawer>
        <div className="dc-core-flex-item dc-core-flex-9 dc-core-flex-md-12">
          <div className={classes.animateFromBottom}>
            <div role="button" tabIndex="0" className={'dc-core-lg-hidden ' + classes.toggleDrawerOnMobile} onClick={this.toggleRoomsDrawer}></div>
            <DCRoom activeRoom={this.state.activeRoom} username={this.state.user}></DCRoom>
          </div>
        </div>
      </div>
    );
  };
}
