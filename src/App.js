import { BrowserRouter } from 'react-router-dom';
import Shell from './containers/Shell/Shell';
import './App.css';

function App() {
  return (
    <BrowserRouter>
      <Shell></Shell>
    </BrowserRouter>
  );
}

export default App;
