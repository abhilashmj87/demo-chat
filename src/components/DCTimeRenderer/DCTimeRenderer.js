import React, { Component } from 'react';
import classes from './DCTimeRenderer.module.css';

export default class DCTimeRenderer extends Component {
  constructor(props) {
    super(props);
    this.intervalHolder = {};
    this.state = {
      elapsedMinutes: 0,
    };
  }


  componentDidMount() {
    this.intervalHolder = setInterval(() => this.setState({ elapsedMinutes: this.state.elapsedMinutes + 1 }), 60000);
  }

  componentWillUnmount() {
    clearInterval(this.intervalHolder);
  };

  render() {
    return (
      <div title={this.props.textBeforeTime + this.state.elapsedMinutes + ' Minutes'} className={classes.smallText}>{this.props.textBeforeTime + this.state.elapsedMinutes + ' Minutes'}</div>
    );
    
  };
}
