import classes from './DCInput.module.css';

function DCInput(props) {
  let comp = {};
  const handleInputChange = (ev) => {
    props.onchange(ev);
  };

  switch (props.template) {
    case 'textarea':
      comp = <textarea title={props.placeholder} placeholder={props.placeholder} className={classes.input_container + ' ' + props.className} onChange={handleInputChange}></textarea>
      break;
    default: 
      comp = <input title={props.placeholder} placeholder={props.placeholder} className={classes.input_container + ' ' + props.className} type="text" onChange={handleInputChange} />
      break;
  }

  return comp;
}

export default DCInput;
