import classes from './DCPageHeader.module.css';

const DCPageHeader = (props) => {
  return (
    <header className={classes.pageHeader + ' ' + props.className}>
      <div className={classes.header + ' dc-core-align-center'}>
        <h1 className='dc-core-padding-sm dc-core-align-center'>{props.headerTitle}</h1>
        <p className="dc-core-align-center">{props.headerSubtitle?.join(', ')}</p>
      </div>
    </header>
  );
}

export default DCPageHeader;