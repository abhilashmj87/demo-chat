import React from 'react';
import classes from './NavigationItem.module.css';

const navigationItem = ( props ) => {
    return (
        <li tabIndex="0" role="link" className={classes.navigationItem + ' ' + props.selected} datakey={props.dataKey}>
            {props.children}
        </li>
    );
}

export default navigationItem;