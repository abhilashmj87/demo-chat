import React from 'react';
import classes from './DCList.module.css';
import NavigationItem from './NavigationItem/NavigationItem';
import DCEmptyState from '../../components/DCEmptyState/DCEmptyState';

const DCList = (props) => {
    const processNavItems = () => {
        const emptyKey = "fetching";
        let navItems = [];
        if (props.data === undefined) {
            throw new Error("Data needs to be passed to the DCList component.");
        } else if (props.data.length === 0) {
            navItems.push(<DCEmptyState key={emptyKey}></DCEmptyState>);
        } else {
            navItems = [];
            props.data.forEach(item => {
                let selectedClass ='';
                if (props.activeItem === item.id) {
                    selectedClass = classes.selected;
                };
                navItems.push(<NavigationItem selected={selectedClass} key={item.id} dataKey={item.id}>{item.name}</NavigationItem>);
            });
        }

        return navItems;
    };

    const processLIClick = (ev) => {
        document.querySelector('.'+classes.selected).classList.remove(classes.selected);
        ev.target.classList.add(classes.selected);
        props.action(ev);
    }

    return (
        <ul className={classes.navigationItems} onClick={processLIClick}>
            {processNavItems()}
        </ul>
    );
};

export default DCList;