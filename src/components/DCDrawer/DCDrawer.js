import classes from './DCDrawer.module.css';

function DCDrawer(props) {
  const prepClass = () => {
    let classString = classes.drawerContainer + ' ' + classes[props.theme] + ' ' + props.className;
    if (props.initialAnimation && props.initialAnimation === "animate") {
      classString += ' ' + classes.openAnimation;
    }

    return classString;
  };

  return (
    <div className={prepClass()}>
      {props.children}
    </div>
  );
}

export default DCDrawer;
