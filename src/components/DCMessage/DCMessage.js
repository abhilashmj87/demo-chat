import classes from './DCMessage.module.css';

const DCMessage = (props) => {
  let messageContainerClass = classes.msgContainer + ' ';
  let modifiedName = '';

  // TODO: Make this check a little more stricter
  if (props.currentUser === props.message.name) {
    messageContainerClass += classes.end;
    modifiedName = 'You';
  } else {
    modifiedName = props.message.name;
  }
  return (
    <div className={messageContainerClass}>
      <div title={modifiedName} className={classes.msgName}>{modifiedName}</div>
      <div>{props.message.message}</div>
    </div>
  );
}

export default DCMessage;
