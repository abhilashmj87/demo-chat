import classes from './DCButton.module.css';

function DCButton(props) {
  const buttonClass = classes.buttonCore + ' ' + classes[props.size];
  return (
    <button className={buttonClass} disabled={props.disable} onClick={props.onclick}>{typeof props.children === 'string' ? props.children : ' '}</button>
  );
}

export default DCButton;
