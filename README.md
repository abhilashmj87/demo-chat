### Steps to run the app

I've used npm to create the react app instead of yarn. Please follow these steps to run the app:

Pre-requisites: Node v7 or above, npm v6 or above. 

1. cd <project_folder>
2. npm i
3. npm run start

On a separate terminal / Cli

4. cd <project_folder>
5. npm run server

---

#### CSS organization

You'll notice that there are two sets of CSS. One set belonging to the respective components. I've bound them to CSS modules so they're auto-namespaced to the component by the build system. The other set of CSS is present in App.css which are general utility CSS that anyone can use and are applied at a global level. For this, I thought of two approaches here - 1. Use display: grid, 2. Use display: flex .

Both the approaches seemed scalable and perfect for the task. I stuck to approcah 2, mainly because I've used flex layout many times and was more comfortable with flex than grid layout. 

With the flex layout, I decided to create a 12 column layout. If you look at my css, I have classes like dc-core-flex-1, dc-core-flex-2 etc. These classes are the 12 column layout classes where flex-1 means 1 column, flex-2 means 2 columns and so on. I took this same approach certain utility css like margins and padding. I created classes like dc-core-margin-sm, dc-core-margin-md etc so that I could re-use them whenever I wanted. Again, due to time constraints, I created some classes only. The overall idea for my css is this: 

1. Have a 12-column layout for your layout.
2. Have common utility classes for margin and padding defined using these rules:
    * Create classes for three main sizes sm (small), md (medium) and lg (large).
    * Create classes for the above size for all edges - top, bottom, start and end. I call them start and end because start can be left for left-to-right direction and can be right for right-to-left direction. 

3. Also create classes for horizontal and vertical sides.
4. Create all flex classes like classes for justify-content, align-items etc.
5. Create media queries for your targeted screens.
6. For anything that can be re-usable or can be changed at a later time (like spacing), create css variables.

I understand that sometimes, css variables might not work (for example: if you are coding for IE). But, with more and more people using modern browsers and a very small population using old browsers helps me use the best of the best features in CSS without compromising the functionality of the app. I used caniuse.com to see if my css approach was supported in all major mobile browsers, and it was. You will also notice that I haven't used any vendor prefixes (like -webkit- or -moz-). This is because I tried writing CSS that was generally available in all browsers without prefixes. It is a good idea to have prefixes in order to support a wide variety of browsers.

Thank you.